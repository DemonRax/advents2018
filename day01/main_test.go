package main

import "testing"

func Test_sum(t *testing.T) {
	for _, test := range []struct {
		desc string
		in   []string
		out  string
	}{
		{
			desc: "empty",
			out:  "0",
		},
		{
			desc: "example 1",
			in:   []string{"+1", "+1", "+1"},
			out:  "3",
		},
		{
			desc: "example 2",
			in:   []string{"+1", "+1", "-2"},
			out:  "0",
		},
		{
			desc: "example 3",
			in:   []string{"-1", "-2", "-3"},
			out:  "-6",
		},
	} {
		t.Run(test.desc, func(t *testing.T) {
			if got := sum(test.in); got != test.out {
				t.Errorf("want %q\ngot\n%q", test.out, got)
			}
		})
	}
}

func Test_first(t *testing.T) {
	for _, test := range []struct {
		desc string
		in   []string
		out  string
	}{
		{
			desc: "example 1",
			in:   []string{"+1", "-1"},
			out:  "0",
		},
		{
			desc: "example 2",
			in:   []string{"+3", "+3", "+4", "-2", "-4"},
			out:  "10",
		},
		{
			desc: "example 3",
			in:   []string{"-6", "+3", "+8", "+5", "-6"},
			out:  "5",
		},
		{
			desc: "example 4",
			in:   []string{"+7", "+7", "-2", "-7", "-4"},
			out:  "14",
		},
	} {
		t.Run(test.desc, func(t *testing.T) {
			if got := first(test.in); got != test.out {
				t.Errorf("want %q\ngot\n%q", test.out, got)
			}
		})
	}
}
