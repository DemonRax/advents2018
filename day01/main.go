package main

import (
	"fmt"
	"strconv"

	"gitlab.com/DemonRax/advents2018/util"
)

func main() {
	list := util.ReadFile("input.txt")
	result := sum(list)
	fmt.Println(result)
	first := first(list)
	fmt.Println(first)
}

func first(nums []string) string {
	var sum int
	seen := map[int]struct{}{0: {}}
	for {
		for _, num := range nums {
			n, _ := strconv.Atoi(num)
			sum += n
			_, ok := seen[sum]
			if ok {
				return strconv.Itoa(sum)
			}
			seen[sum] = struct{}{}
		}
	}
}

func sum(nums []string) string {
	var sum int
	for _, num := range nums {
		n, _ := strconv.Atoi(num)
		sum += n
	}
	return strconv.Itoa(sum)
}
