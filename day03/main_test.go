package main

import (
	"strconv"
	"testing"
)

func Test_overlap(t *testing.T) {
	for _, test := range []struct {
		claims []string
		want,
		wantNon int
	}{
		{
			claims:  []string{"#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"},
			want:    4,
			wantNon: 3,
		},
		{
			claims:  []string{"#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#33 @ 5,5: 2x2", "#4 @ 3,1: 4x4"},
			want:    16,
			wantNon: 33,
		},
		{
			claims:  []string{"#4 @ 3,1: 4x4", "#33 @ 5,5: 2x2", "#2 @ 3,1: 4x4", "#1 @ 1,3: 4x4"},
			want:    16,
			wantNon: 33,
		},
	} {
		t.Run(strconv.Itoa(test.want), func(t *testing.T) {
			got, gotNon := overlap(test.claims)
			if got != test.want {
				t.Errorf("want %d\ngot\n%d", test.want, got)
			}
			if gotNon != test.wantNon {
				t.Errorf("want non %d\ngot\n%d", test.wantNon, gotNon)
			}
		})
	}
}

func Test_parseClaim(t *testing.T) {
	for _, test := range []struct {
		claim string
		want  claim
	}{
		{
			claim: "",
		},
		{
			claim: "#1 @ 2,3: 4x5",
			want:  claim{i: 1, x: 2, y: 3, w: 4, h: 5},
		},
		{
			claim: "#11515 @ 213,533: 5174x565",
			want:  claim{i: 11515, x: 213, y: 533, w: 5174, h: 565},
		},
	} {
		t.Run(test.claim, func(t *testing.T) {
			if got := parseClaim(test.claim); got != test.want {
				t.Errorf("want %v\ngot\n%v", test.want, got)
			}
		})
	}
}
