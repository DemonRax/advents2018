package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2018/util"
	"regexp"
	"strconv"
)

func main() {
	list := util.ReadFile("input.txt")
	overlap, nonOverlapClaimID := overlap(list)
	fmt.Println(overlap, nonOverlapClaimID)
}

func overlap(claims []string) (int, int) {
	result := 0
	nonMap := make(map[int]struct{}, len(claims))
	type data struct {
		count int
		ids   []int
	}
	m := [1000][1000]data{}
	for _, c := range claims {
		cl := parseClaim(c)
		overlapped := false
		for i := cl.x; i < cl.x+cl.w; i++ {
			for j := cl.y; j < cl.y+cl.h; j++ {
				m[i][j] = data{
					count: m[i][j].count + 1,
					ids:   append(m[i][j].ids, cl.i),
				}
				if m[i][j].count == 1 {
					nonMap[cl.i] = struct{}{}
					continue
				}
				overlapped = true
				if m[i][j].count == 2 {
					result++
				}
				for _, id := range m[i][j].ids {
					delete(nonMap, id)
				}
			}
		}
		if overlapped {
			delete(nonMap, cl.i)
		}
	}
	non := 0
	for id := range nonMap {
		non = id
	}
	return result, non
}

type claim struct {
	i, x, y, w, h int
}

func parseClaim(c string) claim {
	pattern := regexp.MustCompile(`^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$`)
	r := pattern.FindAllSubmatch([]byte(c), -1)
	if r == nil {
		return claim{}
	}
	i, _ := strconv.Atoi(string(r[0][1]))
	x, _ := strconv.Atoi(string(r[0][2]))
	y, _ := strconv.Atoi(string(r[0][3]))
	w, _ := strconv.Atoi(string(r[0][4]))
	h, _ := strconv.Atoi(string(r[0][5]))
	return claim{
		i: i,
		x: x,
		y: y,
		w: w,
		h: h,
	}
}
