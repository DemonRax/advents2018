package util

import (
	"bufio"
	"log"
	"os"
)

func ReadFile(fileName string) []string {
	var lines []string
	fileHandle, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
		return nil
	}
	defer fileHandle.Close()
	scanner := bufio.NewScanner(fileHandle)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines
}