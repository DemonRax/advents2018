package main

import "testing"

func Test_checksum(t *testing.T) {
	for _, test := range []struct {
		desc string
		in   []string
		out  int
	}{
		{
			desc: "empty",
			out:  0,
		},
		{
			desc: "example",
			in:   []string{"abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab"},
			out:  12,
		},
	} {
		t.Run(test.desc, func(t *testing.T) {
			if got := checksum(test.in); got != test.out {
				t.Errorf("want %q\ngot\n%q", test.out, got)
			}
		})
	}
}

func Test_count(t *testing.T) {
	for _, test := range []struct {
		in         string
		two, three int
	}{
		{in: ""},
		{in: "abcdef", two: 0, three: 0},
		{in: "bababc", two: 1, three: 1},
		{in: "abbcde", two: 1, three: 0},
		{in: "abcccd", two: 0, three: 1},
		{in: "aabcdd", two: 1, three: 0},
		{in: "abcdee", two: 1, three: 0},
		{in: "ababab", two: 0, three: 1},
	} {
		t.Run(test.in, func(t *testing.T) {
			gotTwo, gotThree := count(test.in)
			if test.two != gotTwo {
				t.Errorf("want two %d\ngot\n%d", test.two, gotTwo)
			}
			if test.three != gotThree {
				t.Errorf("want three %d\ngot\n%d", test.three, gotThree)
			}
		})
	}
}

func Test_common(t *testing.T) {
	for _, test := range []struct {
		desc string
		in   []string
		out  string
	}{
		{
			desc: "example",
			in:   []string{"abcde","fghij","klmno","pqrst","fguij","axcye","wvxyz"},
			out: "fgij",
		},
	} {
		t.Run(test.desc, func(t *testing.T) {
			if got := common(test.in); got != test.out {
				t.Errorf("want %q\ngot\n%q", test.out, got)
			}
		})
	}
}

func Test_compare(t *testing.T) {
	for _, test := range []struct {
		s1, s2, want string
	}{
		{
			s1: "abcde",
			s2: "abbbe",
			want: "abe",
		},
		{
			s1: "abcde",
			s2: "zzzzz",
			want: "",
		},
		{
			s1: "abcde",
			s2: "abcde",
			want: "abcde",
		},
	} {
		t.Run(test.want, func(t *testing.T) {
			if got := compare(test.s1, test.s2); got != test.want {
				t.Errorf("want %q\ngot\n%q", test.want, got)
			}
		})
	}
}
