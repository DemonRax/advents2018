package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2018/util"
)

func main() {
	list := util.ReadFile("input.txt")
	checksum := checksum(list)
	fmt.Println(checksum)
	common := common(list)
	fmt.Println(common)
}

func checksum(input []string) int {
	var twos, threes int
	for _, line := range input {
		two, three := count(line)
		twos += two
		threes += three
	}
	return twos * threes
}

func count(s string) (two, three int) {
	runes := make(map[rune]int, len(s))
	for _, r := range s {
		runes[r]++
	}
	for _, b := range runes {
		if b == 2 {
			two = 1
		}
		if b == 3 {
			three = 1
		}
	}
	return two, three
}

func common(input []string) string {
	for i := 0; i < len(input)-1; i++ {
		for j := i+1; j < len(input); j++ {
			common := compare(input[i], input[j])
			if len(common) == len(input[i])-1 {
				return common
			}
		}
	}
	return ""
}

func compare(s1, s2 string) string {
	result := ""
	for i := range s1 {
		if s1[i] == s2[i] {
			result += string(s1[i])
		}
	}
	return result
}
