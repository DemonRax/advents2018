package main

import (
	"fmt"
	"regexp"
	"sort"
	"strconv"

	"gitlab.com/DemonRax/advents2018/util"
)

type sleep struct{ start, stop int }

func main() {
	list := util.ReadFile("input.txt")
	str1, str2 := zzz(list)
	fmt.Println(str1, str2)
}

func zzz(list []string) (int, int) {
	const (
		fa = "falls asleep"
		wu = "wakes up"
	)

	sort.Strings(list)
	pattern := regexp.MustCompile(`^\[(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d)] (.*)$`)
	shiftPattern := regexp.MustCompile(`^Guard #(\d+) begins shift$`)
	guards := make(map[int][]sleep)

	var start, id int

	for _, entry := range list {
		r := pattern.FindAllSubmatch([]byte(entry), -1)
		m, _ := strconv.Atoi(string(r[0][5]))
		msg := string(r[0][6])

		switch msg {
		case fa:
			start = m
		case wu:
			guards[id] = append(guards[id], sleep{start, m})
		default:
			r = shiftPattern.FindAllSubmatch([]byte(msg), -1)
			id, _ = strconv.Atoi(string(r[0][1]))
		}
	}

	var max, maxID int
	var maxMin, maxCount, maxMinID int
	for id, g := range guards {
		slept := 0
		for _, s := range g {
			slept += s.stop - s.start
		}
		if slept > max {
			maxID = id
			max = slept
		}
		min, count := maxMinute(g)
		if count > maxCount {
			maxCount = count
			maxMin = min
			maxMinID = id
		}
	}
	mm, _ := maxMinute(guards[maxID])
	return maxID * mm, maxMinID * maxMin
}

func maxMinute(sleep []sleep) (int, int) {
	minutes := make(map[int]int, 60)
	for _, slept := range sleep {
		for m := slept.start; m < slept.stop; m++ {
			minutes[m]++
		}
	}

	maxMin, maxCount := 0, 0
	for m, c := range minutes {
		if c > maxCount {
			maxCount = c
			maxMin = m
		}
	}
	return maxMin, maxCount
}
