package main

import (
	"strconv"
	"testing"
)

func Test_zzz(t *testing.T) {
	for _, test := range []struct {
		input []string
		want1, want2 int
	}{
		{
			input: []string{
				"[1518-11-01 00:00] Guard #10 begins shift",
				"[1518-11-01 00:05] falls asleep",
				"[1518-11-01 00:25] wakes up",
				"[1518-11-01 00:30] falls asleep",
				"[1518-11-01 00:55] wakes up",
				"[1518-11-01 23:58] Guard #99 begins shift",
				"[1518-11-02 00:40] falls asleep",
				"[1518-11-02 00:50] wakes up",
				"[1518-11-03 00:05] Guard #10 begins shift",
				"[1518-11-03 00:24] falls asleep",
				"[1518-11-03 00:29] wakes up",
				"[1518-11-04 00:02] Guard #99 begins shift",
				"[1518-11-04 00:36] falls asleep",
				"[1518-11-04 00:46] wakes up",
				"[1518-11-05 00:03] Guard #99 begins shift",
				"[1518-11-05 00:45] falls asleep",
				"[1518-11-05 00:55] wakes up",
			},
			want1:    240,
			want2:    4455,
		},
	} {
		t.Run(strconv.Itoa(test.want1), func(t *testing.T) {
			got1, got2 := zzz(test.input)
			if got1 != test.want1 {
				t.Errorf("want 1 %d\ngot\n%d", test.want1, got1)
			}
			if got2 != test.want2 {
				t.Errorf("want 2 %d\ngot\n%d", test.want2, got2)
			}
		})
	}
}